package com.serebit.logkat.writers

import com.serebit.logkat.LogLevel

/**
 * An implementation of [MessageWriter] that contains multiple LogWriters. This writer passes
 * the log messages through to its internal writers, allowing for multiple log outputs.
 */
class MultiWriter(val writers: List<MessageWriter>) : MessageWriter {
    constructor(vararg writers: MessageWriter) : this(writers.toList())

    /** Calls each the [MessageWriter.write] function of each of this MultiWriter's [writers]. */
    override fun write(message: String, level: LogLevel): Unit = writers.forEach { it.write(message, level) }
}

/**
 * A convenience function to merge two [MessageWriter]s into a single [MultiWriter]. The result will be either a new
 * [MultiWriter], or in the case where the two operands are the same instance, the first operand.
 */
operator fun MessageWriter.plus(other: MessageWriter): MessageWriter = when {
    this === other -> this
    this is MultiWriter && other is MultiWriter -> MultiWriter(writers + other.writers)
    this is MultiWriter -> MultiWriter(writers + other)
    other is MultiWriter -> MultiWriter(other.writers + this)
    else -> MultiWriter(this, other)
}
